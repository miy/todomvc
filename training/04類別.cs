﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _04_Class
    {
        public void Main()
        {
            var person = new Person("hello") { Name = "雲流", Age = 100 };
            person.Talk();
            person.Eat();
        }

        public IEnumerable<IPerson> GetPeople()
        {
            return new List<IPerson>() {
                new Person(){ Name = "雲流", Age=100, Gender = "男" },
                new Person(){ Name = "小白", Age=10, Gender = "男" },
                new Person(){ Name = "中白", Age=20, Gender = "女" },
                new Person(){ Name = "大白", Age=30, Gender = "女" },
                new Person2() { Name = "火星人" , Age = 200 },
                new Person2() { Name = "土星人" , Age = 300 }
            };
        }
    }

    public class Person : IPerson, IBehavior, IDisposable
    {
        private readonly string _value;

        public string Gender { set; get; }
        public string Name { set; get; }
        public int Age { set; get; }

        public Person() { }

        public Person(string value)
        {
            this._value = value;
            Console.WriteLine("Person is created with " + value);
        }

        ~Person()
        {
            Dispose(false);
            Console.WriteLine("Person is disposed");
        }

        public void Eat()
        {
            Console.WriteLine(Name + " eat");
        }

        public void Talk()
        {
            Console.WriteLine(Name + " talk");
        }

        #region IDisposable Support
        private bool disposedValue = false; // 偵測多餘的呼叫

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 處置受控狀態 (受控物件)。
                }

                // TODO: 釋放非受控資源 (非受控物件) 並覆寫下方的完成項。
                // TODO: 將大型欄位設為 null。

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

    public class Person2 : IPerson
    {
        public string Gender { set; get; }
        public string Name { set; get; }
        public int Age { set; get; }
    }
}
