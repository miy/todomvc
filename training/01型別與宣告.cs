﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _01_Type
    {
        public void Main()
        {
            //簡單型別
            sbyte mynumber1 = 127;

            short mynumber2 = 32767;

            int mynumber3 = 2147483647;
            int? nullableNumber = null;

            long mynumber4 = 9223372036854775807;

            char mychar = 'a';

            float myfloat = 0.0f;

            double mydouble = 0.0;

            decimal mydecimal = 0.0m;

            bool mybool = true;

            //列舉
            Week week = Week.Monday;

            //結構
            Person person = new Person()
            {
                Name = "雲流",
                Age = 100
            };

            //類別
            Location location = new Location((str) => Console.WriteLine(str), "華山");

            //字串
            string myString = "hello world";

            //陣列
            string[] myArray = { "hello", "world" };
        }

        public enum Week
        {
            Sunday,
            Monday
        }

        public struct Person
        {
            public string Name { set; get; }
            public int Age { set; get; }
        }

        public class Location
        {
            public Location(ShowName showName, string name)
            {
                this.Name = name;
                showName.Invoke(this.Name);
            }

            private string Name { set; get; }
        }

        public interface ILocation
        {
            string Name { set; get; }
        }

        public delegate void ShowName(string name);
    }
}
