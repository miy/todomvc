﻿using mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvc.Controllers
{
    public class TodoMVCController : Controller
    {
        private readonly mvctrainingEntities _db = new mvctrainingEntities();

        // GET: TodoMVC
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetTodo()
        {
            return Json(_db.TodoMVC.ToList(),JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SyncTodo(IEnumerable<TodoMVC> todos)
        {
            var oldDatas = _db.TodoMVC.AsQueryable();
            _db.TodoMVC.RemoveRange(oldDatas);
            if (todos != null && todos.Count() > 0)
                _db.TodoMVC.AddRange(todos);

            string result = string.Empty;
            try
            {
                _db.SaveChanges();
                result = "成功";
            }
            catch (Exception ex)
            {
                result = "失敗";
            }
            return Json(result);
        }
    }
}