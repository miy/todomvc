﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _06抽象虛擬覆寫修飾詞
    {

    }

    public abstract class AbstractPerson
    {
        public abstract int Age { set; get; }
    }

    public class VirtualPerson
    {
        public virtual int Age { set; get; }

        public static string GetGender()
        {
            return "男";
        }
    }

    public class Man1 : AbstractPerson
    {
        // 必須覆寫
        public override int Age { set; get; }
    }

    public class Man2 : VirtualPerson
    {
        // 選擇覆寫
        // public override int Age { set; get; }
    }
}
