﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _07類型測試運算子
    {
        public void Main()
        {
            object o1 = "hello";
            object o2 = 123;

            // as 
            string t1 = o1 as string;
            Console.WriteLine("t1 as " + t1);

            string t2 = o2 as string;
            Console.WriteLine("t2 as " + t2);

            // is 
            if (o1 is string t3)
                Console.WriteLine("t3 is " + t3);
            if (o2 is string t4)
                Console.WriteLine("t4 is " + t4);


            //var people = new _04_Class().GetPeople().ToList();
            //var earthling = people.Where(x => x is Person && x.Age > 20)
            //    .Select(x =>
            //    {
            //        Person person = x as Person;
            //        return new { x.Name, x.Age };
            //    }).ToList();

            //earthling.ForEach(x =>
            //{
            //    Console.WriteLine(x);
            //});
        }
    }
}
