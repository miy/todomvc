﻿class TodoListData {
    constructor(){
        this.todoListData = [];
    }

    addItem(item){
        this.todoListData.push(item);
    }
    
    setTodoListData(list){
        this.todoListData = list;
    }

    getTodoListData(){
        return this.todoListData;
    }
}

export { TodoListData };