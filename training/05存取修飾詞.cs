﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _05存取修飾詞
    {
        public void Main()
        {
            var test = new Test();
            test.TestA();
            test.TestB();
            //test.TestC(); 
            //test.TestD();
        }
    }

    public class Test
    {
        public void Main()
        {
            TestA();
            TestB();
            TestC(); 
            TestD(); 
        }
        public void TestA()
        {
            Console.WriteLine("public + public");
        }
        internal void TestB()
        {
            Console.WriteLine("public + internal");
        }
        protected void TestC()
        {
            Console.WriteLine("public + protected");
        }
        private void TestD()
        {
            Console.WriteLine("public + private");
        }
    }

    public class Test2 : Test
    {
        public void Main()
        {
            TestA();
            TestB();
            TestC();
            //TestD();
        }
    }
}
