﻿import { $getElement,$delegate } from './helper.js';
import { templete } from './templete.js';
import { TodoListData } from './data.js';
import { Service } from './service.js';

const ENTER_CODE = 13;

class View {
    constructor() {
        const _view = this;
        //--module--//
        this.templete = new templete();
        this.dataProvider = new TodoListData();
        this.service = new Service();
        //----//
        this.todoItem = $getElement('.new-todo');
        this.todoList = $getElement('.todo-list');
        this.showTodoListData = [];
        this.filterType = '';

        this.todoItem.addEventListener('keydown', function (e) {
            if (e.keyCode == ENTER_CODE) _view.addItem();
        });
        $getElement('.sync').addEventListener('click', (e) => _view.syncData());
        this.getData();
        this.bindItem();
        this.changeSelected();
    }
    //--public CRUD--//
    addItem() {
        if (!this.todoItem.value) return;
        let item = {
            id: Date.now(),
            title: this.todoItem.value,
            completed: false
        };
        this.dataProvider.addItem(item);
        this.filter(this.filterType);
        this.todoItem.value = '';
    }
    removeItem(id) {
        this.dataProvider.setTodoListData(this.dataProvider.getTodoListData().filter(x=>x.id != id));
        this.filter(this.filterType);
    }
    removeItems(ids) {
        this.dataProvider.setTodoListData(this.dataProvider.getTodoListData().filter(x=>!ids.includes(x.id.toString())));
        this.filter(this.filterType);
    }
    clearCompleted() {
        let comItems = document.querySelectorAll('.toggle:checked');
        let ids = [];
        for(var comItem of comItems) {
            ids.push(comItem.parentElement.parentElement.getAttribute('data-id').toString());
        }
        this.removeItems(ids);
    }
    checkItem(id) {
        let item = this.dataProvider.getTodoListData().find(x=>x.id == id);
        item.completed = !item.completed;
        this.filter(this.filterType);
    }
    renderItem() {
        this.todoList.innerHTML = this.templete.itemList(this.showTodoListData);
        let showBlock = $getElement('.main');
        showBlock.style['display'] = this.dataProvider.getTodoListData().length > 0 ? 'block' : 'none';
        this.bindItem();
    }
    bindItem() {
        const _view = this;
        let todoBlockItems = $getElement('.todo-list').querySelectorAll('li');
        for (let todoItem of todoBlockItems) {
            let id = todoItem.getAttribute('data-id');
            $getElement('.destroy', todoItem).addEventListener('click', (e) => _view.removeItem(id));
            $getElement('.toggle', todoItem).addEventListener('click', (e) => _view.checkItem(id));
            todoItem.addEventListener('dblclick', function (e) {
                if (!$getElement('input.edit')) {
                    let editItem = _view.dataProvider.getTodoListData().find(x=>x.id == id);
                    todoItem.classList.add('editing');
                    let node = document.createElement("input");
                    node.value = editItem.title;
                    node.classList.add('edit');
                    todoItem.appendChild(node);
                    node.focus();
                    $getElement('.edit', todoItem).addEventListener('blur', function (e) {
                        editItem.title = e.target.value;
                        _view.filter(_view.filterType);
                    });
                }
            });
            todoItem.addEventListener('keydown', function (e) {
                if (e.keyCode == ENTER_CODE) e.target.blur();
            });
        }
        $getElement('.clear-completed').addEventListener('click', function (e) {
            _view.clearCompleted();
        });
    }
    //--public filter--//
    filter(type) {
        let data = this.dataProvider.getTodoListData();
        if (type == 'active') { this.showTodoListData = data.filter(x=>!x.completed); }
        else if (type == 'completed') { this.showTodoListData = data.filter(x=>x.completed); }
        else { this.showTodoListData = data }
        $getElement('.todo-count').innerHTML = data.filter(x=>!x.completed).length + ' items left';
        this.renderItem();
    }
    changeSelected() {
        let _view = this;
        let filters = $getElement('.filters');
        for (var filter of filters.querySelectorAll('li')) {
            filter.addEventListener('click', function (e) {
                let type = e.target.getAttribute('data-type');
                filters.querySelectorAll('li > a').forEach(x=>x.classList.remove('selected'));
                e.target.classList.add('selected');
                _view.filterType = type;
                _view.filter(type);
            });
        }
    }
    syncData(){
        let data = this.dataProvider.getTodoListData().map(x=> { return { Todo:x.title, IsCompleted:x.completed }});
        this.service.syncData(data);
    } 

    getData(){
        const _view = this;
        $.get('/todomvc/GetTodo',function(re){
            if(re) {
                re.forEach(x=>{_view.dataProvider.addItem({id:x.Id,title:x.Todo,completed:x.IsCompleted});});
                _view.filter(this.filterType);
            }
        });
    }
};

export { View } ;
