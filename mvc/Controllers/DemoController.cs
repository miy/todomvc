﻿using mvc.Filters;
using mvc.Models;
using mvc.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace mvc.Controllers
{
    [ControllerFilter]
    public class DemoController : Controller
    {
        private readonly mvctrainingEntities _db = new mvctrainingEntities();
        // GET: Demo
        public async Task<ActionResult> Index()
        {
            //ViewData["message1"] = "world";
            //ViewBag.message2 = "hello";
            ViewBag.Role = "admin";
            //TempData["message3"] = "sir";
            return View(await GetAllAsync());
        }

        public ActionResult Create()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult Create(Person model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                _db.Person.Add(model);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            return View(GetAll_EF().FirstOrDefault(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult Edit(Person model)
        {
            if (!ModelState.IsValid)
                return View(model);
            try
            {
                _db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var model = GetAll_EF().FirstOrDefault(x => x.Id == id);
            if (model == null)
                return RedirectToAction("Index");
            try
            {
                _db.Person.Remove(model);
                _db.SaveChanges();
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        [AuthorizeFilter]
        [ActionFilter]
        public ActionResult TestFilter()
        {
            object txt = "hello";
            //var number = (int)txt;
            return Json("safe", JsonRequestBehavior.AllowGet);
        }

        private List<Person> GetAll_ADO()
        {
            var queryString = "select*from person";
            var cs = "data source=(local);initial catalog=mvctraining;integrated security=True;MultipleActiveResultSets=True;";
            var result = new List<Person>();
            using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cs))
            {
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(queryString, cn))
                {
                    System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            result.Add(new Person
                            {
                                Name = reader["Name"] as string,
                                Id = (int)reader["Id"],
                                Age = (int)reader["Age"]
                            });
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            return result;
        }

        private List<Person> GetAll_EF()
        {
            var per = (from a in _db.Person
                       join b in _db.Pet on a.Id equals b.PersonId into bb
                       from bbb in bb.DefaultIfEmpty()
                       select new { a.Id, a.Age, a.Name, PetKind = bbb.Kind, PetName = bbb.Name } into s
                       group s by s.Id into aaaa
                       select new
                       {
                           Id = aaaa.Key,
                           aaaa.FirstOrDefault().Name,
                           aaaa.FirstOrDefault().Age,
                           Pets = aaaa.Where(y => y.PetKind != null).Select(y => new
                           {
                               y.PetName,
                               y.PetKind
                           })
                       }
                      ).ToList();
            var per2 = _db.Person.Join(_db.Pet, x => x.Id, y => y.PersonId, (person, pet) => new
            {
                person.Id,
                person.Name,
                person.Age,
                PetKind = pet.Kind,
                PetName = pet.Name
            }).GroupBy(x => x.Id).Select(x => new
            {
                x.Key,
                x.FirstOrDefault().Name,
                x.FirstOrDefault().Age,
                Pets = x.Where(y => y.PetKind != null).Select(y => new
                {
                    y.PetName,
                    y.PetKind
                })
            }).ToList();

            var persons = _db.Person.GroupJoin(_db.Pet, person => person.Id, pet => pet.PersonId, (person, pet) => new
            {
                person.Id,
                person.Name,
                person.Age,
                pet
            }).ToList();
            var result = _db.Person.ToList();
            return result;
        }

        private Task<List<PersonViewModel>> GetAllAsync()
        {
            return _db.Person.GroupJoin(_db.Pet, person => person.Id, pet => pet.PersonId, (person, pet) => new PersonViewModel
            {
                Id = person.Id,
                Name = person.Name,
                Age = person.Age,
                //Pet = pet
            }).ToListAsync();
        }
    }
}