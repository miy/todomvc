﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _09選擇陳述式
    {
        public void Main()
        {
            string text = null, result = null;
            if (text == null)
            {
                result = "text is null";
            }
            else
            {
                result = "text is not null";
            }
            Console.WriteLine(result);

            object obj = new Person() { Age = 100 };
            switch (obj)
            {
                case Person person when person.Age == 0:
                    Console.WriteLine("person is 0");
                    break;
                case Person person when person.Age == 100:
                    Console.WriteLine("person is 100");
                    break;
                case Person2 person2:
                    Console.WriteLine("obj is Person2");
                    break;
                default:
                    Console.WriteLine("not match");
                    break;
            }
        }
    }
}
