﻿    export function $getElement(selector, scope) {
        return (scope || document).querySelector(selector);
    }
    export function $delegate(target,eventName,callback) {
        target.addEventListener(eventName, callback);
    }

