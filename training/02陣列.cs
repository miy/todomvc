﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _02_Array
    {
        public void Main()
        {
            string[] array1 = new string[4];

            string[,] array2 = { { "0,0", "0,1", "0,2", "0,3" }, { "1,0", "1,1", "1,2", "1,3" } };

            string[][] array3 = { new string[] { "[0][0]", "[0][1]", "[0][2]" }, new string[] { "[1][0]", "[1][1]", "[1][2]", "[1][3]" } };

            Console.WriteLine("array2[0, 1] = " + array2[0, 1]);
            Console.WriteLine("array3[1][2] = " + array3[1][2]);
        }
    }
}
