﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _08算術運算子
    {
        public void Main()
        {
            // 運算子(2/4) – 算術運算子
            int A = 10;
            int B = 20;

            Console.WriteLine("A = 10 , y = 20");
            Console.WriteLine("A + B = " + (A + B));
            Console.WriteLine("A - B = " + (A - B));
            Console.WriteLine("A * B = " + (A * B));
            Console.WriteLine("A / B = " + (A / B));
            Console.WriteLine("A++ = " + ++A);
            //Console.WriteLine("A-- = " + --A);

            // 運算子(3/4) – 關係運算子、邏輯運算子
            int x = 0, y = 1;
            Console.WriteLine("x = 0 , y = 1");
            Console.WriteLine("x > y is " + (x > y));
            Console.WriteLine("x < y is " + (x < y));
            Console.WriteLine("x == y is " + (x == y));
            Console.WriteLine("x != y is " + (x != y));
            Console.WriteLine("x < y && x == y is " + (x < y && x == y));

            // 運算子(4/4) – Null聯合運算子、條件運算子
            string text = null;
            string result = text ?? "text is null";
            Console.WriteLine(result);
            string result2 = text != null ? "text is not null" : "text is null";
            Console.WriteLine(result2);
        }
    }
}
