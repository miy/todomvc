﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _10反覆運算陳述式
    {
        public void Main()
        {
            int n = 0;
            do
            {
                Console.Write(n);
                n++;
            } while (n < 5);
            Console.WriteLine();

            n = 0;
            while (n < 5)
            {
                Console.Write(n);
                n++;
            }
            Console.WriteLine();

            for (int i = 0; i < 5; i++)
            {
                Console.Write(i);
            }
            Console.WriteLine();

            var numbers = new int[] { 3, 14, 15, 92, 6 };
            foreach (int number in numbers)
            {
                //if (number == 92)
                //continue;
                //if (number == 6)
                //break;
                Console.Write("{0} ", number);
            }
        }
    }
}
