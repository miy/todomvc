﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public class _11例外況狀處理陳述式
    {
        public void Main()
        {
            try
            {
                Sample(null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("do finally");
            }

            void Sample(string value)
            {
                if (value == null)
                    throw new Exception("value is null");
            }
        }
    }
}
