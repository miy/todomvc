﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace mvc.Models.ViewModels
{
    public class PersonViewModel
    {
        public int Id { set; get; }
        [Display(Name = "名稱")]
        [Required(ErrorMessage = "輸入名稱拜託")]
        public string Name { set; get; }
        [Required]
        [Display(Name = "年齡")]
        public int Age { set; get; }
    }
}