﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training
{
    public interface IPerson
    {
        string Name { set; get; }
        int Age { set; get; }
    }

    public interface IBehavior
    {
        void Eat();
        void Talk();
    }
}
